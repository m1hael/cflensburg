/*-------------------------------------------------------------------*/
/*                                                                   */
/*  Compile options:                                                 */
/*                                                                   */
/*    CrtCmd  Cmd( CHGVLDLE )                                        */
/*            Pgm( CBX2053 )                                         */
/*            VldCkr( CBX2053V )                                     */
/*            MsgF( CBX205M )                                        */
/*            SrcMbr( CBX2053X )                                     */
/*            HlpPnlGrp( CBX2053H )                                  */
/*            HlpId( *CMD )                                          */
/*            Aut( *EXCLUDE )                                        */
/*                                                                   */
/*-------------------------------------------------------------------*/
          Cmd      Prompt( 'Change Validation List Entry' )

          Parm     VLDL        Q0001                       +
                   Min( 1 )                                +
                   Choice( *NONE )                         +
                   Prompt( 'Validation list' )

          Parm     ENTID       E0001                       +
                   Min( 1 )                                +
                   Prompt( 'Entry ID' )

          Parm     ENCDTA      E0002                       +
                   Dft( *SAME)                             +
                   SngVal( *SAME)                          +
                   Prompt( 'Encryption data' )

          Parm     ENTDTA      E0003                       +
                   Dft( *SAME)                             +
                   SngVal( *SAME)                          +
                   Prompt( 'Entry data' )

          Parm     ENCDTAOPT   *Char       1               +
                   Rstd( *YES )                            +
                   Dft( *SAME )                            +
                   SpcVal(( *VFYONLY '0' )                 +
                          ( *VFYFIND '1' )                 +
                          ( *SAME    '*' ))                +
                   PmtCtl( P0004 )                         +
                   Prompt( 'Encryption data option' )

          Parm     ENTIDHEX    *Hex       100              +
                   PmtCtl( P0001 )                         +
                   InlPmtLen( 50 )                         +
                   Prompt( 'Entry ID hexadecimal' )

          Parm     ENCDTAHEX   *Hex       256              +
                   PmtCtl( P0002 )                         +
                   InlPmtLen( 50 )                         +
                   Prompt( 'Encrypted data hexadecimal' )

          Parm     ENTDTAHEX   *Hex       256              +
                   PmtCtl( P0003 )                         +
                   InlPmtLen( 132 )                        +
                   Prompt( 'Entry data hexadecimal' )

 Q0001:   Qual                 *Name       10              +
                   Min( 1 )                                +
                   Expr( *YES )

          Qual                 *Name       10              +
                   Dft( *LIBL )                            +
                   SpcVal(( *LIBL    )                     +
                          ( *CURLIB  *CURLIB ))            +
                   Expr( *YES )                            +
                   Prompt( 'Library' )

 E0001:   Elem                 *Char      100              +
                   Min( 1 )                                +
                   SpcVal( *HEX )                          +
                   Expr( *YES )                            +
                   Vary( *YES *INT2 )                      +
                   Case( *MIXED )                          +
                   InlPmtLen( 32 )                         +
                   Prompt( 'Entry ID' )

          Elem                 *Int4                       +
                   Dft( *DFT )                             +
                   Range( 1 65534 )                        +
                   SpcVal(( *DFT  0 ) ( *HEX 65535 ))      +
                   Expr( *YES )                            +
                   Prompt( 'Coded character set identifier' )

 E0002:   Elem                 *Char      600              +
                   Min( 1 )                                +
                   SpcVal(( *HEX ) ( *NONE ))              +
                   Expr( *YES )                            +
                   Vary( *YES *INT2 )                      +
                   Case( *MIXED )                          +
                   InlPmtLen( 32 )                         +
                   Prompt( 'Encryption data' )

          Elem                 *Int4                       +
                   Dft( *DFT )                             +
                   Range( 1 65534 )                        +
                   SpcVal(( *DFT  0 ) ( *HEX 65535 ))      +
                   Expr( *YES )                            +
                   Prompt( 'Coded character set identifier' )

 E0003:   Elem                 *Char     1000              +
                   Min( 1 )                                +
                   SpcVal(( *HEX ) ( *NONE ))              +
                   Expr( *YES )                            +
                   Vary( *YES *INT2 )                      +
                   Case( *MIXED )                          +
                   InlPmtLen( 80 )                         +
                   Prompt( 'Entry data' )

          Elem                 *Int4                       +
                   Dft( *DFT )                             +
                   Range( 1 65534 )                        +
                   SpcVal(( *DFT  0 ) ( *HEX 65535 ))      +
                   Expr( *YES )                            +
                   Prompt( 'Coded character set identifier' )


 P0001:   PmtCtl   Ctl( ENTID )                            +
                   Cond(( *EQ *HEX ))

 P0002:   PmtCtl   Ctl( ENCDTA )                           +
                   Cond(( *EQ *HEX ))

 P0003:   PmtCtl   Ctl( ENTDTA )                           +
                   Cond(( *EQ *HEX ))

 P0004:   PmtCtl   Ctl( ENCDTA )                           +
                   Cond(( *NE *SAME ))


          Dep      Ctl( &ENTID *EQ *HEX )                  +
                   Parm(( ENTIDHEX ))                      +
                   NbrTrue( *EQ 1 )                        +
                   MsgId( CBX0101 )

          Dep      Ctl( &ENCDTA *EQ *HEX )                 +
                   Parm(( ENCDTAHEX ))                     +
                   NbrTrue( *EQ 1 )                        +
                   MsgId( CBX0102 )

          Dep      Ctl( &ENTDTA *EQ *HEX )                 +
                   Parm(( ENTDTAHEX ))                     +
                   NbrTrue( *EQ 1 )                        +
                   MsgId( CBX0103 )

          Dep      Ctl( &ENCDTA *EQ *NONE )                +
                   Parm(( ENCDTAOPT ))                     +
                   NbrTrue( *EQ 0 )                        +
                   MsgId( CBX0104 )

          Dep      Ctl( *ALWAYS )                          +
                   Parm(( &ENCDTA *EQ *SAME )              +
                        ( &ENTDTA *EQ *SAME ))             +
                   NbrTrue( *LT 2 )                        +
                   MsgId( CBX0105 )

          Dep      Ctl( &ENCDTA *EQ *SAME )                +
                   Parm(( ENCDTAOPT ))                     +
                   NbrTrue( *EQ 0 )                        +
                   MsgId( CBX0106 )

