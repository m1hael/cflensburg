/*-------------------------------------------------------------------*/
/*                                                                   */
/*  Compile options:                                                 */
/*                                                                   */
/*    CrtCmd Cmd( WRKVLDL )                                          */
/*           Pgm( CBX2061 )                                          */
/*           SrcMbr( CBX2061X )                                      */
/*           VldCkr( CBX2061V )                                      */
/*           Allow( *INTERACT *IPGM *IREXX *IMOD )                   */
/*           AlwLmtUsr( *NO )                                        */
/*           HlpPnlGrp( CBX2061H )                                   */
/*           HlpId( *CMD )                                           */
/*           Aut( *EXCLUDE )                                         */
/*                                                                   */
/*-------------------------------------------------------------------*/
          Cmd      Prompt( 'Work with Validation Lists' )

          Parm     VLDL        Q0001                            +
                   Min( 1 )                                     +
                   Choice( *NONE )                              +
                   Prompt( 'Validation list' )

          Parm     ORDER       *Char       10                   +
                   Dft( *VLDL )                                 +
                   Rstd( *YES )                                 +
                   SpcVal(( *VLDL )                             +
                          ( *LIB  ))                            +
                   Expr( *YES )                                 +
                   Prompt( 'Sort order' )


 Q0001:   Qual                 *Generic    10                   +
                   SpcVal(( *ALL ))                             +
                   Min( 1 )                                     +
                   Expr( *YES )

          Qual                 *Name                            +
                   Dft( *LIBL )                                 +
                   SpcVal(( *LIBL    )                          +
                          ( *CURLIB  )                          +
                          ( *USRLIBL )                          +
                          ( *ALLUSR  )                          +
                          ( *ALL     ))                         +
                   Expr( *YES )                                 +
                   Prompt( 'Library' )

