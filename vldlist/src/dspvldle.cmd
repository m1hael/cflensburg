/*-------------------------------------------------------------------*/
/*                                                                   */
/*  Compile options:                                                 */
/*                                                                   */
/*    CrtCmd  Cmd( DSPVLDLE )                                        */
/*            Pgm( CBX204 )                                          */
/*            VldCkr( CBX204V )                                      */
/*            SrcMbr( CBX204X )                                      */
/*            HlpPnlGrp( CBX204H )                                   */
/*            HlpId( *CMD )                                          */
/*                                                                   */
/*-------------------------------------------------------------------*/
          Cmd      Prompt( 'Display Validation List Entry' )

          Parm     VLDL        Q0001                       +
                   Min( 1 )                                +
                   Choice( *NONE )                         +
                   Prompt( 'Validation list' )

          Parm     ENTID       E0001                       +
                   Min( 1 )                                +
                   Prompt( 'Entry ID' )

          Parm     OUTFMT      *Char       3               +
                   Rstd( *YES )                            +
                   Dft( *CHAR )                            +
                   SpcVal(( *CHAR 'CHR' ) ( *HEX 'HEX' ))  +
                   Prompt( 'Output format' )

          Parm     OUTPUT      *Char       3               +
                   Rstd( *YES )                            +
                   Dft( * )                                +
                   SpcVal(( * 'DSP' ) ( *PRINT 'PRT' ))    +
                   Prompt( 'Output' )

          Parm     ENTIDHEX    *Hex       100              +
                   PmtCtl( P0001 )                         +
                   InlPmtLen( 50 )                         +
                   Prompt( 'Entry ID hexadecimal' )

 Q0001:   Qual                 *Name       10              +
                   Min( 1 )                                +
                   Expr( *YES )

          Qual                 *Name       10              +
                   Dft( *LIBL )                            +
                   SpcVal(( *LIBL    )                     +
                          ( *CURLIB  *CURLIB ))            +
                   Expr( *YES )                            +
                   Prompt( 'Library' )

 E0001:   Elem                 *Char      100              +
                   Min( 1 )                                +
                   SpcVal( *HEX )                          +
                   Expr( *YES )                            +
                   Vary( *YES *INT2 )                      +
                   Case( *MIXED )                          +
                   InlPmtLen( 32 )                         +
                   Prompt( 'Entry ID' )

          Elem                 *Int4                       +
                   Dft( *DFT )                             +
                   Range( 1 65534 )                        +
                   SpcVal(( *DFT  0 ) ( *HEX 65535 ))      +
                   Expr( *YES )                            +
                   Prompt( 'Coded character set identifier' )

 P0001:   PmtCtl   Ctl( ENTID )                            +
                   Cond(( *EQ *HEX ))

