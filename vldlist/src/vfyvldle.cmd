/*-------------------------------------------------------------------*/
/*                                                                   */
/*  Compile options:                                                 */
/*                                                                   */
/*    CrtCmd  Cmd( VFYVLDLE )                                        */
/*            Pgm( CBX2052 )                                         */
/*            VldCkr( CBX2052V )                                     */
/*            MsgF( CBX205M )                                        */
/*            SrcMbr( CBX2052X )                                     */
/*            HlpPnlGrp( CBX2052H )                                  */
/*            HlpId( *CMD )                                          */
/*            Aut( *EXCLUDE )                                        */
/*                                                                   */
/*-------------------------------------------------------------------*/
          Cmd      Prompt( 'Verify Validation List Entry' )

          Parm     VLDL        Q0001                       +
                   Min( 1 )                                +
                   Choice( *NONE )                         +
                   Prompt( 'Validation list' )

          Parm     ENTID       E0001                       +
                   Min( 1 )                                +
                   Prompt( 'Entry ID' )

          Parm     ENCDTA      E0002                       +
                   Min( 1 )                                +
                   Prompt( 'Encryption data' )

          Parm     ENTIDHEX    *Hex       100              +
                   PmtCtl( P0001 )                         +
                   InlPmtLen( 50 )                         +
                   Prompt( 'Entry ID hexadecimal' )

          Parm     ENCDTAHEX   *Hex       256              +
                   PmtCtl( P0002 )                         +
                   InlPmtLen( 50 )                         +
                   Prompt( 'Encrypted data hexadecimal' )

 Q0001:   Qual                 *Name       10              +
                   Min( 1 )                                +
                   Expr( *YES )

          Qual                 *Name       10              +
                   Dft( *LIBL )                            +
                   SpcVal(( *LIBL    )                     +
                          ( *CURLIB  *CURLIB ))            +
                   Expr( *YES )                            +
                   Prompt( 'Library' )

 E0001:   Elem                 *Char      100              +
                   Min( 1 )                                +
                   SpcVal( *HEX )                          +
                   Expr( *YES )                            +
                   Vary( *YES *INT2 )                      +
                   Case( *MIXED )                          +
                   InlPmtLen( 32 )                         +
                   Prompt( 'Entry ID' )

          Elem                 *Int4                       +
                   Dft( *DFT )                             +
                   Range( 1 65534 )                        +
                   SpcVal(( *DFT  0 ) ( *HEX 65535 ))      +
                   Expr( *YES )                            +
                   Prompt( 'Coded character set identifier' )

 E0002:   Elem                 *Char      600              +
                   Min( 1 )                                +
                   SpcVal(( *HEX ))                        +
                   Expr( *YES )                            +
                   Vary( *YES *INT2 )                      +
                   Case( *MIXED )                          +
                   InlPmtLen( 32 )                         +
                   Prompt( 'Encryption data' )

          Elem                 *Int4                       +
                   Dft( *DFT )                             +
                   Range( 1 65534 )                        +
                   SpcVal(( *DFT  0 ) ( *HEX 65535 ))      +
                   Expr( *YES )                            +
                   Prompt( 'Coded character set identifier' )


 P0001:   PmtCtl   Ctl( ENTID )                            +
                   Cond(( *EQ *HEX ))

 P0002:   PmtCtl   Ctl( ENCDTA )                           +
                   Cond(( *EQ *HEX ))


          Dep      Ctl( &ENTID *EQ *HEX )                  +
                   Parm(( ENTIDHEX ))                      +
                   NbrTrue( *EQ 1 )                        +
                   MsgId( CBX0101 )

          Dep      Ctl( &ENCDTA *EQ *HEX )                 +
                   Parm(( ENCDTAHEX ))                     +
                   NbrTrue( *EQ 1 )                        +
                   MsgId( CBX0102 )

