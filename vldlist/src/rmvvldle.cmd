/*-------------------------------------------------------------------*/
/*                                                                   */
/*  Compile options:                                                 */
/*                                                                   */
/*    CrtCmd  Cmd( RMVVLDLE )                                        */
/*            Pgm( CBX2054 )                                         */
/*            VldCkr( CBX2054V )                                     */
/*            MsgF( CBX205M )                                        */
/*            SrcMbr( CBX2054X )                                     */
/*            HlpPnlGrp( CBX2054H )                                  */
/*            HlpId( *CMD )                                          */
/*            Aut( *EXCLUDE )                                        */
/*                                                                   */
/*-------------------------------------------------------------------*/
          Cmd      Prompt( 'Remove Validation List Entry' )

          Parm     VLDL        Q0001                       +
                   Min( 1 )                                +
                   Choice( *NONE )                         +
                   Prompt( 'Validation list' )

          Parm     ENTID       E0001                       +
                   Min( 1 )                                +
                   Prompt( 'Entry ID' )

          Parm     ENTIDHEX    *Hex       100              +
                   PmtCtl( P0001 )                         +
                   InlPmtLen( 50 )                         +
                   Prompt( 'Entry ID hexadecimal' )


 Q0001:   Qual                 *Name       10              +
                   Min( 1 )                                +
                   Expr( *YES )

          Qual                 *Name       10              +
                   Dft( *LIBL )                            +
                   SpcVal(( *LIBL    )                     +
                          ( *CURLIB  *CURLIB ))            +
                   Expr( *YES )                            +
                   Prompt( 'Library' )

 E0001:   Elem                 *Char      100              +
                   Min( 1 )                                +
                   SpcVal( *HEX )                          +
                   Expr( *YES )                            +
                   Vary( *YES *INT2 )                      +
                   Case( *MIXED )                          +
                   InlPmtLen( 32 )                         +
                   Prompt( 'Entry ID' )

          Elem                 *Int4                       +
                   Dft( *DFT )                             +
                   Range( 1 65534 )                        +
                   SpcVal(( *DFT  0 ) ( *HEX 65535 ))      +
                   Expr( *YES )                            +
                   Prompt( 'Coded character set identifier' )

 E0002:   Elem                 *Char      600              +
                   Min( 1 )                                +
                   SpcVal(( *HEX ))                        +
                   Expr( *YES )                            +
                   Vary( *YES *INT2 )                      +
                   Case( *MIXED )                          +
                   InlPmtLen( 32 )                         +
                   Prompt( 'Encryption data' )

          Elem                 *Int4                       +
                   Dft( *DFT )                             +
                   Range( 1 65534 )                        +
                   SpcVal(( *DFT  0 ) ( *HEX 65535 ))      +
                   Expr( *YES )                            +
                   Prompt( 'Coded character set identifier' )


 P0001:   PmtCtl   Ctl( ENTID )                            +
                   Cond(( *EQ *HEX ))

          Dep      Ctl( &ENTID *EQ *HEX )                  +
                   Parm(( ENTIDHEX ))                      +
                   NbrTrue( *EQ 1 )                        +
                   MsgId( CBX0101 )

