/*-------------------------------------------------------------------*/
/*                                                                   */
/*  Compile options:                                                 */
/*                                                                   */
/*    CrtCmd Cmd( WRKVLDLE )                                         */
/*           Pgm( CBX207 )                                           */
/*           SrcMbr( CBX207X )                                       */
/*           Allow( *INTERACT *IPGM *IREXX *IMOD )                   */
/*           VldCkr( CBX207V )                                       */
/*           HlpPnlGrp( CBX207H )                                    */
/*           HlpId( *CMD )                                           */
/*           PrdLib( <library> )                                     */
/*                                                                   */
/*-------------------------------------------------------------------*/
             Cmd        Prompt( 'Work with Validation List Ents' )

             Parm       VLDL        Q0001                  +
                        Min( 1 )                           +
                        Choice( *NONE )                    +
                        Prompt( 'Validation list' )


Q0001:      Qual                    *Name       10         +
                        Min( 1 )                           +
                        Expr( *YES )

            Qual                    *Name       10         +
                        Dft( *LIBL )                       +
                        SpcVal(( *LIBL    )                +
                               ( *CURLIB  *CURLIB ))       +
                        Expr( *YES )                       +
                        Prompt( 'Library' )

