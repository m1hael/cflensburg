/*-------------------------------------------------------------------*/
/*                                                                   */
/*  Compile options:                                                 */
/*                                                                   */
/*    CrtCmd  Cmd( CLRVLDL )                                         */
/*            Pgm( CBX2062 )                                         */
/*            VldCkr( CBX2062V )                                     */
/*            SrcMbr( CBX2062X )                                     */
/*            HlpPnlGrp( CBX2062H )                                  */
/*            HlpId( *CMD )                                          */
/*            Aut( *EXCLUDE )                                        */
/*                                                                   */
/*-------------------------------------------------------------------*/
          Cmd      Prompt( 'Clear Validation List' )

          Parm     VLDL        Q0001                       +
                   Min( 1 )                                +
                   Choice( *NONE )                         +
                   Prompt( 'Validation list' )


 Q0001:   Qual                 *Name       10              +
                   Min( 1 )                                +
                   Expr( *YES )

          Qual                 *Name       10              +
                   Dft( *LIBL )                            +
                   SpcVal(( *LIBL    )                     +
                          ( *CURLIB  *CURLIB ))            +
                   Expr( *YES )                            +
                   Prompt( 'Library' )

