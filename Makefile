#
# API by Example from Carsten Flensburg
#

#-------------------------------------------------------------------------------
# User-defined part start
#

# BIN_LIB is the destination library (needs to be in the library list)
BIN_LIB=QGPL

# Target Release
TGTRLS=*CURRENT

#
# User-defined part end
#-------------------------------------------------------------------------------


all: compile bind commands clean

clean:
	$(MAKE) -C vldlist clean $*

compile:
	$(MAKE) -C vldlist compile $*

commands:
	$(MAKE) -C vldlist commands $*
	
bind:
	$(MAKE) -C vldlist bind $*

release:
	$(MAKE) -C vldlist release $*
	
.PHONY:
